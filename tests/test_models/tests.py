import unittest

from Tank import Tank
from Dot import Dot
from Vector import Vector


class MyTestCase(unittest.TestCase):
    def test_move(self):
        """ Тест движения """
        t = Tank(Vector(12, 5), Vector(-7, 3))
        t.move()
        self.assertEqual(t.get_position().get_x(), Vector(5, 8).get_x())
        self.assertEqual(t.get_position().get_y(), Vector(5, 8).get_y())

    def test_none_position(self):
        """ Нельзя двигать объект без определенного местоположения """
        t = Tank(None, Vector(-7, 3))
        with self.assertRaises(Exception) as context:
            t.move()
        self.assertEqual("unsupported operand type(s) for +: 'NoneType' and 'Vector'", str(context.exception))


    def test_none_velocity(self):
        """ Нельзя двигать объект без определенного вектора скорости """
        t = Tank(Vector(12, 5), None)
        with self.assertRaises(Exception) as context:
            t.move()
        self.assertEqual("bad operand type for abs(): 'NoneType'", str(context.exception))

    def test_rotate(self):
        """ Поворот на 90 градусов """
        t = Tank(Vector(12, 5), Vector(0, 3))
        t.rotate()
        self.assertEqual(t.get_velocity().get_x(), Vector(3, 0).get_x())
        self.assertEqual(t.get_velocity().get_y(), Vector(3, 0).get_y())

    def test_no_velocity(self):
        """ Нельзя двигать объект с нулевым вектором скорости """
        t = Tank(Vector(12, 5), Vector(0, 0))
        with self.assertRaises(Exception) as context:
            t.move()
        self.assertEqual("Объект имеет нулевой вектор скорости", str(context.exception))

    def test_no_move(self):
        """ Нельзя двигать объект без соответствующего интерфейса """
        d = Dot(Vector(0, 1))
        with self.assertRaises(Exception) as context:
            d.move()
        self.assertEqual("'Dot' object has no attribute 'move'", str(context.exception))


if __name__ == '__main__':
    unittest.main()
