#!/usr/bin/env python
# -*- coding: utf-8 -*-


import unittest
from test_models.tests import MyTestCase
import xmlrunner


def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(MyTestCase))
    return test_suite


if __name__ == '__main__':


    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output='test-reports', verbosity=2),
        failfast=False,
        buffer=False,
        catchbreak=False)

