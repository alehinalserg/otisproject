#!/usr/bin/env python
# -*- coding: utf-8 -*-


class MyException(Exception):
    """ Произвольные исключения """
    def __init__(self, text):
        self.txt = text
