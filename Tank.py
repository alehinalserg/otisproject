#!/usr/bin/env python
# -*- coding: utf-8 -*-

from interfaces.IMoveble import Move
from interfaces.IRotateble import Rotate
from Vector import Vector


class Tank(Move, Rotate):
    """
        Класс Танк. Реализован, как объект, умеющий двигаться,
        поворачиваться и стрелять (пока не реализовано)
    """
    def __init__(self, position=Vector(0, 0), velocity=Vector(0, 0)):
        """
        Конструктор класса
        :param position: координата объекта
        :param velocity: Направление
         и скорость
        """
        super().__init__()
        for base_class in self.__class__.__bases__:
            super(base_class, self).__init__()

        self.position = position
        self.velocity = velocity


if __name__ == '__main__':

    t = Tank(Vector(0, 1), Vector(1, 0))
    t.move()
    print(t)
    t.move()
    print(t)
    t.move()
    print(t)
    t.rotate()
    print(t)
    t.rotate()
    print(t)
    t.rotate()
    print(t)
    t.rotate()

    print(t)