#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import hypot, sin, cos, radians


class Vector:
    """
    Класс реализует вектор на плоскости и операции над векторами
    """
    def __init__(self, x, y):
        """
        Конструктор класса
        :param x: координата по оси абцисс
        :param y: координата по оси ординат
        """
        self.__x = x
        self.__y = y

    def __repr__(self):
        """ Вывод в консоль """
        return f'Vector({self.__x}, {self.__y})'

    def __abs__(self):
        """ Возвращает длину вектора """
        return hypot(self.__x, self.__y)

    def __bool__(self):
        """ Является ли вектор нулевым """
        return bool(abs(self))

    def __add__(self, other):
        """
        Сложение векторов
        :param other: Вектор
        :return:
        """

        x = self.__x + other.__x
        y = self.__y + other.__y
        return Vector(x, y)

    def get_x(self):
        """ Координата по оси абцисс """
        return self.__x

    def get_y(self):
        """ Координата по оси ординат """
        return self.__y

    def rotate(self, angle=90):
        """ Поворот вектора на 90 град"""

        if self.__y == 0:
            angle = 360 - angle

        cs = cos(radians(angle))
        sn = sin(radians(angle))
        rx = round((self.__x * cs + self.__y * sn))
        ry = round((self.__x * sn + self.__y * cs))
        self.__x = rx
        self.__y = ry
        return Vector(self.__x, self.__y)


if __name__ == '__main__':

    v1 = Vector(x=0, y=2)
    print(v1)
    v1.rotate()
    print(v1)
    v1.rotate()
    print(v1)
    v1.rotate()
    print(v1)
    v1.rotate()
    print(v1)