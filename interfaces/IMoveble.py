#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Vector import Vector
from MyException import MyException


class IMoveble:
    """
    Интерфейс для всего, что движется на плоскости прямолинейно
    """
    def __init__(self, position=Vector(0, 0), velocity=Vector(0, 0)):
        """
        Конструктор класса
        :param position: координата объекта
        :param velocity: Направление движения и скорость
        """
        self.position = position
        self.velocity = velocity

    def get_position(self):
        """ Возвращает координаты объекта """
        return self.position

    def get_velocity(self):
        """ Возвращает вектор движения объекта. Направление и скорость """
        return self.velocity

    def set_position(self, new_pos):
        """ Изменение кординат объекта """
        self.position = new_pos


class Move(IMoveble):
    """
    Класс реализует плоское прямолинейное движение
    """
    def __init__(self):
        """ Конструктор класса """
        super().__init__()

    def move(self):
        """ Движение """
        try:
            if abs(self.get_velocity()) == 0:
                raise MyException('Объект имеет нулевой вектор скорости')
        except Exception:
            raise
        self.position = self.get_position() + self.get_velocity()

    def __repr__(self):
        """ Вывод в консоль """
        return f'{self.get_position()}, {self.velocity}'

