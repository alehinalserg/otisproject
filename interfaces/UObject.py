#!/usr/bin/env python
# -*- coding: utf-8 -*-


class UObject:
    """ Универсальный объект """

    def get_property(self, key: str):
        """ Проверка наличия атрибута объекта """
        return key in self.__dict__

    def set_property(self, key: str, value):
        """ Присвоение атрибутунового значения """
        self.__dict__[key] = value
