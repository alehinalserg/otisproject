#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Vector import Vector


class IRotable:
    """ Классреализует поворот любого объекта """
    def __init__(self, position=Vector(0, 0), velocity=Vector(0, 0)):
        """
        Конструктор класса
        :param position: координата объекта
        :param velocity: Направление движения и скорость
        """
        self.position = position
        self.velocity = velocity

    def get_position(self):
        """ Возвращает координаты объекта """
        return self.position

    def get_velocity(self):
        """ Возвращает текущий вектор скорости """
        return self.velocity

    def set_velocity(self, new_value):
        """ Задает текущий вектор скорости """
        self.velocity = new_value


class Rotate(IRotable):
    """ Поворот объекта на 90 градусов """
    def __init__(self):
        super().__init__()

    def rotate(self):
        """set position"""
        velocity = self.get_velocity().rotate()
        self.set_velocity(velocity)

    def __repr__(self):
        """ Вывод в консоль """
        return f'{self.get_position()}, {self.velocity}'
