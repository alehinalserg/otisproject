#!/usr/bin/env python
# -*- coding: utf-8 -*-


class MovableAdapter:
    """ Адаптер. Зачем нужен - не понятно """
    def __init__(self, obj):
        self.obj = obj

    def get_position(self):
        self.obj.get_property('position')

    def set_position(self, new_value):
        self.obj.set_property('position', new_value)

    def get_velocity(self):
        self.obj.get_property('velocity')
