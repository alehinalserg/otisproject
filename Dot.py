#!/usr/bin/env python
# -*- coding: utf-8 -*-

from interfaces.IRotateble import Rotate
from Vector import Vector


class Dot(Rotate):
    """
        Класс Долговременная огневая точка:). Реализован, как объект, умеющий поворачиваться,
        но не имеющий возможности передвигаться
    """
    def __init__(self, position=Vector(0, 0), velocity=Vector(0, 0)):
        """
        Конструктор класса
        :param position: координата объекта
        :param velocity: Направление и скорость
        """
        super().__init__()
        for base_class in self.__class__.__bases__:
            super(base_class, self).__init__()

        self.position = position
        self.velocity = velocity


if __name__ == '__main__':

    d = Dot(Vector(0, 1), Vector(1, 0))
    d.rotate()
    print(d)
    d.rotate()
    print(d)
    d.rotate()
    print(d)
    d.rotate()
    print(d)
